A1) Implement ‘two-sided stack’ with methods *peekFirst, peekLast, removeFirst, removeLast, addFirst, addLast* + tests

A2)  Atoi(String) parse string to integer (also negative). Create tests and handle each cases (positive, negative,
preceded by zeros, multi-numbers)

A3)  Implement function to calculate power of number, function_power (number, power), test for negative, positive and
zero number

A4)  find the median in two arrays funkcja_median (2 tabeles of ints primitiv)

A5)  Hash map implementation e.g. put i get without using hash map.

A6)  Reverse the string (algorithm, not using reverse method)

A7)  Finding numbers in the array / the number of pairs which values add up to the specified value

A8)  Median in the set of numbers

A9)  There is a list of strings: task is to create a set of sets, with the same anagram (‘dog’ i ‘God’ would be the
same, as alphabetically sorted anagram is ‘dgo’)

A10) Finding the shortest route from point A to point B

A11) Finding the second largest value in the table

A12) Search for the most frequently occurring IP address in http server logs. A function that searches for the most
frequently occurring IP

A13) Find the first unique letter in "test", "aabbdc". Solution: "e" in the first case, "d" in the second case. It is
not the first letter from an alphabetical point of view, but in the word.

A14) Having a given alphabet as a string, find in the given sentence all characters from the alphabet that do not appear
in this sentence and return them sorted alphabetically.

A15) Average grade of students

    * given input array of students with grades like so:
    * String[][] scores = {{"Bob","85"},{"Mark","100"},{"Charles","63"},{"Mark","34"}};
    * calculate best average grade. If the average is decimal floor it down to the nearest integer value.
    * Grades can be positive and negative integer values.
    * Write down some edge cases test
    
    · Scope: Finding the best mean in the table + Average, highest mean for each student + Grouping, sorting, max (lambda)

A16) There is input in form of list like : ["X" "R1 R2" "R3 R4" "T" "N1" "N2"] where:
* One the first index ("X") is integer which is equal to amount of ranges,
* Next there are ranges ("R1 R2" "R3 R4") these ranges contains integers and describes low and high possible number in range
* After ranges there is number ("T") which describes amount of numbers to check
* After "T" there are numbers to check

The task is to print in array for each number to check if number is in any range or not. The output should look like 
["Yes" "No" "Yes" "No"], "Yes" - for number which belongs to at least one range, "No" - for number which does not belong to any of ranges.

A17) For a given value, find the shortest possible word (in terms of the number of letters). Each letter of the alphabet a number is assigned as follows:

    A=1
    
    B=2*A+2=4
    
    C=3*B+3=15
    
    D=4*C+4=64
    
    ...
    
    The value of the word made from letters is equal to the sum of the values of these letters, that is:
    
    AAD=1+1+64=66
    
    So, for example, if the word is to have the value of 25, it can be:
    1. AAAAAAAAAAAAAAAAAAAAAAAAA
    or
    2. BBBBAAAAAAAAA
    etc, but the shortest is: CBBAA