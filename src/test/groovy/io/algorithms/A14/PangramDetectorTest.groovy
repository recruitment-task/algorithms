package io.algorithms.A14

import spock.lang.Specification

class PangramDetectorTest extends Specification {

    //A14) Mając dany alfabet jako string, znaleźć w podanym zdaniu wszystkie znaki z alfabetu,
    // które w tym zdaniu nie występują i zwrócić je posortowane alfabetycznie –
    // do tego załączam rozwiązanie (kod załączony w 14. Pangram detector)

    def 'should return whole alphabet when text is empty or blank'() {
        when:
            def result = PanagramDetector.detect(text)

        then:
            result == ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
            noExceptionThrown()

        where:
            text << ["", "  "]
    }

    def 'should return #expectedChars for text = #text'() {
        when:
            def result = PanagramDetector.detect(text)

        then:
            result == expectedChars
            noExceptionThrown()

        where:
            text                                          || expectedChars
            "abcdefghijklmnopqrstuvwxyz"                  || []
            "The quick brown fox jumps over the lazy dog" || []
            "Pack my box with five dozen liquor jugs."    || []
            "Jackdaws love my big sphinx of quartz."      || []
            "The five boxing wizards jump quickly."       || []
            "Bright vixens jump; dozy fowl quack."        || []
            "Sphinx of black quartz, judge my vow."       || []
            "This is not a pangram."                      || ['B', 'C', 'D', 'E', 'F', 'J', 'K', 'L', 'Q', 'U', 'V', 'W', 'X', 'Y', 'Z']
            "I am not a pangram"                          || ['B', 'C', 'D', 'E', 'F', 'H', 'J', 'K', 'L', 'Q', 'S', 'U', 'V', 'W', 'X', 'Y', 'Z']
    }


}
