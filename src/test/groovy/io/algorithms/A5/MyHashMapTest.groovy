package io.algorithms.A5

import spock.lang.Specification

class MyHashMapTest extends Specification {

    /**
     * 1. should put and get element from map
     * 2. should update element of specific key and get updated
     */

    def 'should put and get element from map on specific key'() {
        given:
            final def map = new MyHashMap<Integer, String>()

        when:
            map.put(key, element)

        then:
            map.get(key).get() == element
            noExceptionThrown()

        where:
            key || element
            1   || "Element"
            2   || "Element2"
    }

    def 'should put some element with different key and return then from map'() {
        given:
            final def map = new MyHashMap<Integer, String>()

        when:
            map.put(1, "Element1")

        then:
            map.get(1).get() == "Element1"

        and:
            map.put(2, "Element2")

        then:
            map.get(2).get() == "Element2"

        and:
            map.put(3, "Element3")

        then:
            map.get(3).get() == "Element3"

        and:
            map.put(11, "Element11")

        then:
            map.get(11).get() == "Element11"

        and:
            map.put(21, "Element21")

        then:
            map.get(21).get() == "Element21"
    }

    def 'should update element on the same key'() {
        given:
            final def map = new MyHashMap<Integer, String>()


        when:
            map.put(1, "Element1")

        then:
            map.get(1).get() == "Element1"

        and:
            map.put(1, "ELEMENT1")

        then:
            map.get(1).get() == "ELEMENT1"
    }
}
