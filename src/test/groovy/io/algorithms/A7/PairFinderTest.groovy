package io.algorithms.A7

import spock.lang.Specification

class PairFinderTest extends Specification {

    def 'should find all pairs in array which sum is equal to 20'() {
        when:
            def result = PairFinder.findInArrayWhichSumEqualTo(array as int[], sum)

        then:
            result == expected
            noExceptionThrown()

        where:
            array                              | sum || expected
            [1, 2, 4, 5, 7, 10, 12, 15, 9]     | 11  || [[4, 7], [2, 9], [1, 10]]
            [34, 72, 103, 5, 43, -893, 970, 9] | 77  || [[5, 72], [-893, 970], [34, 43]]

    }
}
