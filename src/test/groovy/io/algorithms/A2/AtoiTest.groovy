package io.algorithms.A2

import spock.lang.Specification
import spock.lang.Unroll

class AtoiTest extends Specification {

    /**
     * TESTS
     * 1. parse 0 (DONE)
     * 2. parse 2 (DONE)
     * 3. parse 33 (DONE)
     * 4. parse 33333 (DONE)
     * 5. parse -2 (DONE)
     * 6. parse -5000 (DONE)
     * 7. parse 0050 (DONE)
     * 8. parse null expect IllegalArgsException (DONE)
     * 9. parse abc expect IAE (DONE)
     * 10. parse blank or empty test expect IAE
     */

    @Unroll
    def 'should convert text #text to integer = #value'() {
        expect:
            Atoi.parse(text) == value

        where:
            text    | value
            "0"     | 0
            "2"     | 2
            "33"    | 33
            "33333" | 33333
            "-2"    | -2
            "-5000" | -5000
            "0050"  | 50
    }

    @Unroll
    def 'should throw IllegalArgumentException when input is #text'() {
        when:
            Atoi.parse(text)

        then:
            thrown(IllegalArgumentException)

        where:
            text << [null, "abc", "", "  "]
    }
}
