package io.algorithms.A12

import spock.lang.Specification
import spock.lang.Unroll

class DominantDetectorTest extends Specification {

    @Unroll
    def 'should detect ip address equals #expectedIp as the most popular in input array #inputArray'() {
        when:
            def result = DominantDetector.detect(inputArray)

        then:
            result == expectedIp
            noExceptionThrown()

        where:
            inputArray                                                                                    || expectedIp
            ["6.0.0.0", "7.0.0.0", "11.0.0.0", "6.0.0.0", "21.0.0.0", "11.0.0.0", "49.0.0.0", "11.0.0.0"] || "11.0.0.0"
            ["11.0.0.0", "6.0.0.0", "7.0.0.0", "11.0.0.0", "6.0.0.0", "21.0.0.0", "11.0.0.0", "49.0.0.0"] || "11.0.0.0"


    }
}
