package io.algorithms.A9

import spock.lang.Specification

class AnagramGrouperTest extends Specification {

    def 'should group words with the same anagram in the set of sets'() {
        given:
            final def setOfWords = Set.of("god", "dog", "anna", "nana", "aann")

        when:
            def groupedWords = AnagramGrouper.group(setOfWords)

        then:
            groupedWords.size() == 2
            groupedWords == Set.of(Set.of("god", "dog"), Set.of("anna", "nana", "aann"))
            noExceptionThrown()
    }

}
