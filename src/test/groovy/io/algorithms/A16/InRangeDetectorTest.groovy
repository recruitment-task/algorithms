package io.algorithms.A16

import spock.lang.Specification
import spock.lang.Unroll

class InRangeDetectorTest extends Specification {

    @Unroll
    def 'should property check if each number is belonging to any of given ranges in input #input'() {
        when:
            def result = InRangeDetector.detect(input)

        then:
            result == expectedDetection
            noExceptionThrown()

        where:
            input                                                                                  || expectedDetection
            ["3", "11 15", "9 10", "2 7", "4", "1", "3", "7", "16"]                                || ["No", "Yes", "Yes", "No"]
            ["5", "1 3", "20 22", "5 11", "15 17", "18 19", "6", "4", "1", "21", "10", "14", "30"] || ["No", "Yes", "Yes", "Yes", "No", "No"]
    }
}
