package io.algorithms.A1

import spock.lang.Shared
import spock.lang.Specification

class TwoSidedStackTest extends Specification {

    private static final String ELEMENT = "element"
    @Shared
    private Stack<String> stack

    def setup() {
        stack = Stacks.twoSided()
    }

    def 'should add element on the first place of stack'() {
        when:
            stack.addFirst(ELEMENT)

        then:
            stack.peekFirst().get() == ELEMENT
            noExceptionThrown()
    }

    def 'should add element on the last place of stack'() {
        when:
            stack.addLast(ELEMENT)

        then:
            stack.peekLast().get() == ELEMENT
            noExceptionThrown()
    }

    def 'element added on the first place into empty stack should be also last element'() {
        when:
            stack.addFirst(ELEMENT)

        then:
            stack.peekFirst().get() == stack.peekLast().get()
            noExceptionThrown()
    }

    def 'element added on the last place into empty stack should be also first element'() {
        when:
            stack.addLast(ELEMENT)

        then:
            stack.peekLast().get() == stack.peekFirst().get()
            noExceptionThrown()
    }

    def 'should set current first element in one element stack as last element after adding new first element'() {
        given:
            final String expectedLastElement = "EXPECTED_LAST_ELEMENT"
            stack.addFirst(expectedLastElement)

        when:
            stack.addFirst(ELEMENT)

        then:
            stack.peekFirst().get() == ELEMENT
            stack.peekLast().get() == expectedLastElement
            noExceptionThrown()
    }

    def 'should set current last element in one element stack as first element after adding new last element'() {
        given:
            final String expectedFirstElement = "EXPECTED_FIRST_ELEMENT"
            stack.addLast(expectedFirstElement)

        when:
            stack.addLast(ELEMENT)

        then:
            stack.peekFirst().get() == expectedFirstElement
            stack.peekLast().get() == ELEMENT
            noExceptionThrown()
    }

    def 'should return Optional.empty() by peek any element of empty stack'() {
        when:
            def result = stack.peekLast()

        then:
            result == Optional.empty()
            noExceptionThrown()

        when:
            result = stack.peekFirst()

        then:
            result == Optional.empty()
    }

    def 'should throw exception when trying to remove first element from empty stack'() {
        when:
            stack.removeFirst()

        then:
            def exception = thrown(IllegalStateException)
            exception.getMessage() == "Stack is empty"
    }

    def 'should throw exception when trying to remove last element from empty stack'() {
        when:
            stack.removeLast()

        then:
            def exception = thrown(IllegalStateException)
            exception.getMessage() == "Stack is empty"
    }

    def 'should remove first element from one element stack'() {
        given:
            stack.addFirst(ELEMENT)

        when:
            stack.removeFirst()

        then:
            stack.peekFirst() == Optional.empty()
            stack.peekFirst() == stack.peekLast()
            noExceptionThrown()
    }

    def 'should remove last element from one element stack'() {
        given:
            stack.addLast(ELEMENT)

        when:
            stack.removeLast()

        then:
            stack.peekLast() == Optional.empty()
            stack.peekLast() == stack.peekFirst()
            noExceptionThrown()
    }

    def 'should set first as a last element after removing last element in two element set'() {
        given:
            final String first = "FIRST"
            final String last = "LAST"
            stack.addFirst(last)
            stack.addFirst(first)

        when:
            stack.removeLast()

        then:
            stack.peekLast().isPresent()
            stack.peekFirst().get() == stack.peekLast().get()
            noExceptionThrown()
    }

    def 'should set last as a first element after removing first element in two element set'() {
        given:
            final String first = "FIRST"
            final String last = "LAST"
            stack.addLast(first)
            stack.addLast(last)

        when:
            stack.removeFirst()

        then:
            stack.peekFirst().isPresent()
            stack.peekLast().get() == stack.peekFirst().get()
            noExceptionThrown()
    }

    def 'should put middle element as a last after removing last element'() {
        given:
            final String firstElement = "FIRST"
            final String middleElement = "MIDDLE"
            final String lastElement = "LAST"
            stack.addFirst(lastElement)
            stack.addFirst(middleElement)
            stack.addFirst(firstElement)

        when:
            stack.removeLast()

        then:
            stack.peekLast().isPresent()
            stack.peekLast().get() == middleElement
            noExceptionThrown()
    }

    def 'should put middle element as a first after removing first element'() {
        given:
            final String firstElement = "FIRST"
            final String middleElement = "MIDDLE"
            final String lastElement = "LAST"
            stack.addLast(firstElement)
            stack.addLast(middleElement)
            stack.addLast(lastElement)

        when:
            stack.removeFirst()

        then:
            stack.peekFirst().isPresent()
            stack.peekFirst().get() == middleElement
            noExceptionThrown()
    }

}
