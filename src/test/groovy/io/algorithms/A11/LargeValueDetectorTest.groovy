package io.algorithms.A11

import spock.lang.Specification
import spock.lang.Unroll

class LargeValueDetectorTest extends Specification {

    @Unroll
    def 'should find second largest value (#expectedValue) in array #array'() {
        when:
            def result = LargeValueDetector.detectSecondLargest(array)

        then:
            result == expectedValue
            noExceptionThrown()

        where:
            expectedValue || array
            4             || [1, 2, 4, 7]
            12            || [1, 2, 5, 7, 9, 10, 12, 15]
    }
}
