package io.algorithms.A13

import spock.lang.Specification
import spock.lang.Unroll

class FirstUniqueLetterDetectorTest extends Specification {

    @Unroll
    def 'should detect letter #expectedLetter as a first unique letter in word #inputWord'() {
        when:
            def result = FirstUniqueLetterDetector.detect(inputWord)

        then:
            result == expectedLetter as char
            noExceptionThrown()

        where:
            inputWord                       || expectedLetter
            "aaabchdfhgdfsh"                || 'b'
            "bbbdsfghasdfghsfghsdfgh"       || 'd'
            "ooooooooooooooooooooooooooooh" || 'h'
    }
}
