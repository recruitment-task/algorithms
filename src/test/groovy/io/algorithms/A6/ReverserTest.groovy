package io.algorithms.A6

import spock.lang.Specification
import spock.lang.Unroll

class ReverserTest extends Specification {

    @Unroll
    def 'should reverse the text #text'() {
        when:
            def result = Reverser.execute(text)

        then:
            result == reversedText
            noExceptionThrown()

        where:
            text             || reversedText
            "Ala ma kota"    || "atok am alA"
            "It is working!" || "!gnikrow si tI"
    }
}
