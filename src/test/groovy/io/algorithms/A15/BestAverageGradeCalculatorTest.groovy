package io.algorithms.A15

import spock.lang.Specification
import spock.lang.Unroll

class BestAverageGradeCalculatorTest extends Specification {

    def 'should calculate best average'() {
        given:
            String[][] scores = [["Bob","85"],["Mark","100"],["Charles","63"],["Mark","34"]]

        when:
            def result = BestAverageGradeCalculator.calculate(scores)

        then:
            result == new BestAverageGradeCalculator.BestAverageGrade("Mark", 100)
            noExceptionThrown()
    }

    @Unroll
    def 'should throw exception when grade value is invalid'() {
        when:
            BestAverageGradeCalculator.calculate((String[][]) scores)

        then:
            thrown(RuntimeException)

        where:
            scores << [
                [["Bob",""],["Mark","100"]],
                [["Bob",null],["Mark","100"]],
                [["Bob","-2147483649"],["Mark","100"]],
                [["Bob","2147483649"],["Mark","100"]],
                [["Bob","123.123"],["Mark","100"]],
                [["Bob","A"],["Mark","100"]],
            ]
    }
}
