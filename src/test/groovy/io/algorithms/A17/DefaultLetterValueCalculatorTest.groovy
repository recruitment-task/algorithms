package io.algorithms.A17

import spock.lang.Specification

class DefaultLetterValueCalculatorTest extends Specification {

    def 'should properly calculate letter value'() {
        given:
            final def alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            final def calculator = new DefaultLetterValueCalculator(alphabet)

        when:
            def result = calculator.calculate(letter as char)

        then:
            result == value
            noExceptionThrown()

        where:
            letter || value
            'A'    || 1
            'B'    || 2 * (1) + 2
            'C'    || 3 * (2 * (1) + 2) + 3
            'D'    || 4 * (3 * (2 * (1) + 2) + 3) + 4
    }
}
