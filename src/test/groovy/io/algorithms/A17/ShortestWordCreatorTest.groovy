package io.algorithms.A17

import spock.lang.Specification
import spock.lang.Unroll

class ShortestWordCreatorTest extends Specification {

    @Unroll
    def 'should produce #shortestText with length equals to #textWeight'() {
        given:
            final def alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            final def wordCreator = new WordCreator(alphabet, new DefaultLetterValueCalculator(alphabet))

        when:
            def result = wordCreator.shortestWord(BigInteger.valueOf(textWeight))

        then:
            result == shortestText
            noExceptionThrown()

        where:
            shortestText || textWeight
            "CBBAA"      || 25
            "DCB"        || 83
            "FDBA"       || 2025
    }
}
