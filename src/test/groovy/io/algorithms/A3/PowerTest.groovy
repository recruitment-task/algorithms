package io.algorithms.A3

import spock.lang.Specification
import spock.lang.Unroll

class PowerTest extends Specification {

    /**
     * Tests
     * 1. 0^2 = 0
     * 2. 2 ^ 2 = 4
     * 3. 3 ^ 3 = 27
     * 4. 2 ^ 10 = 1024
     * 5. (-1)^2 = 1
     * 6. (-3)^3 = -27
     * 7. (-2)^2 = 4
     */

    @Unroll
    def '#base ^ #exponent = #expectedValue'() {
        expect:
            Power.of(base, exponent) == expectedValue

        where:
            base | exponent | expectedValue
            0    | 2        | 0
            2    | 2        | 4
            3    | 3        | 27
            2    | 10       | 1024
            -1   | 2        | 1
            -3   | 3        | -27
            -2   | 2        | 4
    }
}
