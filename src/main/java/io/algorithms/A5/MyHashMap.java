package io.algorithms.A5;

import java.util.Optional;

class MyHashMap<K, E> {

    private final Entry<K, E>[] entries;
    private final int size;

    MyHashMap(int size) {
        this.size = size;
        this.entries = new Entry[this.size];
    }

    MyHashMap() {
        this(10);
    }

    void put(K key, E element) {
        int hash = hashFunction(key);
        Entry<K, E> newEntry = new Entry<>(key, element, null);
        if (entries[hash] == null) {
            entries[hash] = newEntry;
        } else {
            Entry<K, E> current = entries[hash];
            Entry<K, E> previous = null;
            while (current != null) {
                if (current.key.equals(key)) {
                    if (previous == null) {
                        newEntry.next = current.next;
                        entries[hash] = newEntry;
                        break;
                    } else {
                        newEntry.next = current.next;
                        break;
                    }
                }
                previous = current;
                current = current.next;
            }
            if (previous != null) {
                previous.next = newEntry;
            }
        }
    }

    Optional<E> get(K key) {
        int hash = hashFunction(key);
        Entry<K, E> entry = entries[hash];
        while (entry != null) {
            if (entry.key.equals(key)) {
                return Optional.of(entry.value);
            }
            entry = entry.next;
        }
        return Optional.empty();
    }

    private int hashFunction(K key) {
        return key.hashCode() % this.size;
    }

    private static class Entry<K, V> {
        private final K key;
        private final V value;
        private Entry<K, V> next;

        Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

    }
}
