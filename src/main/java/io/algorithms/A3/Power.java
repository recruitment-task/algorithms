package io.algorithms.A3;

class Power {

    static int of(int base, int exponent) {
        if (exponent != 0) {
            return base * of(base, exponent - 1);
        } else {
            return 1;
        }
    }
}
