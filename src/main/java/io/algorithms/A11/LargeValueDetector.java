package io.algorithms.A11;

import java.util.List;

class LargeValueDetector {

    public static int detectSecondLargest(List<Integer> integers) {
        int first = Integer.MIN_VALUE, second = first;
        for (int i = 0; i < integers.size(); i++) {
            int current = integers.get(i);
            if (current > first) {
                second = first;
                first = current;
            } else if (current > second && current != first) {
                second = current;
            }
        }
        return second;
    }
}
