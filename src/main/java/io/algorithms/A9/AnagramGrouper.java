package io.algorithms.A9;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class AnagramGrouper {

    static Set<Set<String>> group(Set<String> words) {
        Map<String, Set<String>> anagramMap = new HashMap<>();
        for (String word : words) {
            String anagram = calculateAnagram(word);
            Set<String> wordGroup = anagramMap.get(anagram);
            if (wordGroup == null) {
                wordGroup = new HashSet<>();
            }
            wordGroup.add(word);
            anagramMap.put(anagram, wordGroup);
        }
        return new HashSet<>(anagramMap.values());
    }


    private static String calculateAnagram(String word) {
        return word.chars()
                .sorted()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
