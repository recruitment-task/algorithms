package io.algorithms.A2;

import java.util.Map;
import java.util.regex.Pattern;

class Atoi {

    private static final Map<Character, Integer> NUMBERS = Map.of(
            '0', 0,
            '1', 1,
            '2', 2,
            '3', 3,
            '4', 4,
            '5', 5,
            '6', 6,
            '7', 7,
            '8', 8,
            '9', 9
    );

    static Integer parse(String text) {
        if (text == null || text.isBlank()) {
            throw new IllegalArgumentException();
        }
        int sum = 0;
        boolean isNegative = text.length() > 0 && text.charAt(0) == '-';
        text = isNegative ? text.substring(1) : text;
        for (int i = 0; i < text.length(); i++) {
            Integer number = NUMBERS.get(text.charAt(i));
            if (number != null) {
                sum = (sum * 10) + number;
            } else {
                throw new IllegalArgumentException();
            }
        }
        return isNegative ? sum * -1 : sum;
    }
}
