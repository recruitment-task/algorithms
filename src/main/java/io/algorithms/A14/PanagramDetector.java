package io.algorithms.A14;

class PanagramDetector {

    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    static char[] detect(String text) {
        if (text.isBlank()) return ALPHABET.toCharArray();
        boolean[] detectedChars = new boolean[26];
        int indexToMark;
        int sum = 26;
        for (int index = 0; index < text.length(); index++) {
            if (sum <= 0) {
                break;
            }
            final char currentChar = text.charAt(index);
            if (currentChar >= 'A' && currentChar <= 'Z') {
                indexToMark = currentChar - 'A';
            } else if (currentChar >= 'a' && currentChar <= 'z') {
                indexToMark = currentChar - 'a';
            } else {
                continue;
            }
            if (!detectedChars[indexToMark]) {
                sum -= 1;
            }
            detectedChars[indexToMark] = true;
        }
        StringBuilder missingLetters = new StringBuilder();
        for (int i = 0; i < detectedChars.length; i++) {
            if (!detectedChars[i]) {
                missingLetters.append(ALPHABET.charAt(i));
            }
        }
        return missingLetters.toString().toCharArray();
    }
}
