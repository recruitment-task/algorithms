package io.algorithms.A12;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DominantDetector {
    static String detect(List<String> ipAddresses) {
        Map<String, Integer> mostFrequentlyMap = new HashMap<>();
        String keyOfDominant = "";
        int maxOccurrences = 0;
        for (String ip : ipAddresses) {
            Integer occurrence = mostFrequentlyMap.get(ip);
            occurrence = occurrence == null ? 1 : occurrence + 1;
            mostFrequentlyMap.put(ip, occurrence);
            if (occurrence > maxOccurrences) {
                maxOccurrences = occurrence;
                keyOfDominant = ip;
            }
        }
        return keyOfDominant;
    }
}
