package io.algorithms.A15;

import java.util.Objects;
import java.util.stream.Stream;

class BestAverageGradeCalculator {

    public static BestAverageGrade calculate(String[][] scores) {
        return Stream.of(scores)
                .map(score -> new BestAverageGrade(score[0], score[1]))
                .max(BestAverageGrade::compareTo)
                .orElse(BestAverageGrade.EMPTY);
    }

    static class BestAverageGrade implements Comparable<BestAverageGrade> {
        final String student;
        final int grade;
        private static final BestAverageGrade EMPTY = new BestAverageGrade("Unknown", 0);

        BestAverageGrade(String student, String grade) {
            if (grade == null) {
                throw new IllegalArgumentException();
            }
            this.student = student;
            this.grade = Integer.parseInt(grade);
        }

        BestAverageGrade(String student, int grade) {
            this.student = student;
            this.grade = grade;
        }

        @Override
        public int compareTo(BestAverageGrade grade) {
            return Integer.compare(this.grade, grade.grade);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BestAverageGrade that = (BestAverageGrade) o;
            return grade == that.grade && Objects.equals(student, that.student);
        }

        @Override
        public int hashCode() {
            return Objects.hash(student, grade);
        }
    }
}
