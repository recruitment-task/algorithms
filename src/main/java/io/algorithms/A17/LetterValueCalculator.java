package io.algorithms.A17;

import java.math.BigInteger;

interface LetterValueCalculator {

    BigInteger calculate(char letter);
}
