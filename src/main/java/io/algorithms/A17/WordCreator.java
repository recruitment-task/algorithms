package io.algorithms.A17;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

class WordCreator {
    
    private final String alphabet;
    private final LetterValueCalculator calculator;

    WordCreator(String alphabet, LetterValueCalculator calculator) {
        this.alphabet = alphabet;
        this.calculator = calculator;
    }

    String shortestWord(BigInteger textWeight) {
        Letter[] letters = prepareLetters();
        int index = alphabet.length() - 1;
        StringBuilder wordBuilder = new StringBuilder();
        while(textWeight.compareTo(BigInteger.ZERO) > 0) {
            final Letter letter = letters[index];
            if (letter.weight.compareTo(textWeight) > 0) {
                index--;
            } else {
                wordBuilder.append(letter.value);
                textWeight = textWeight.subtract(letter.weight);
            }
        }
        return wordBuilder.toString();
    }

    private Letter[] prepareLetters() {
        return alphabet.chars()
                .mapToObj(value -> (char) value)
                .map(character -> new Letter(character, calculator.calculate(character)))
                .toArray(Letter[]::new);
    }

    private static class Letter {
        private final char value;
        private final BigInteger weight;

        Letter(char value, BigInteger weight) {
            this.value = value;
            this.weight = weight;
        }
    }
}
