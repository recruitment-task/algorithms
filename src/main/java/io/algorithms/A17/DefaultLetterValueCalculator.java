package io.algorithms.A17;

import java.math.BigInteger;

class DefaultLetterValueCalculator implements LetterValueCalculator {

    private final String alphabet;

    DefaultLetterValueCalculator(String alphabet) {
        this.alphabet = alphabet;
    }

    public BigInteger calculate(char letter) {
        final int indexOfChar = alphabet.indexOf(letter);
        if (indexOfChar == 0) {
            return BigInteger.ONE;
        } else {
            return BigInteger.valueOf(indexOfChar + 1)
                    .multiply(calculate(alphabet.charAt(indexOfChar - 1)))
                    .add(BigInteger.valueOf(indexOfChar + 1));
        }
    }
}
