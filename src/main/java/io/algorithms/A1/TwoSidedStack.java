package io.algorithms.A1;

import java.util.Optional;

class TwoSidedStack<T> implements Stack<T> {

    private Entry<T> first;
    private Entry<T> last;

    @Override
    public void addFirst(T element) {
        if (this.first == null) {
            this.first = new Entry<>(element, null, this.last == null ? null : this.last);
        } else {
            this.first.previous = new Entry<>(element, null, this.first);
            this.first = this.first.previous;
        }
        if (this.last == null) {
            this.last = this.first;
        }
    }

    @Override
    public void addLast(T element) {
        if (this.last == null) {
            this.last = new Entry<>(element, this.first == null ? null : this.first, null);
        } else {
            this.last.next = new Entry<>(element, this.last, null);
            this.last = this.last.next;
        }
        if (this.first == null) {
            this.first = this.last;
        }
    }

    @Override
    public Optional<T> peekFirst() {
        return Optional.ofNullable(first).map(entry -> entry.value);
    }

    @Override
    public Optional<T> peekLast() {
        return Optional.ofNullable(last).map(entry -> entry.value);
    }

    @Override
    public void removeFirst() {
        if (this.first == null) {
            throw new IllegalStateException("Stack is empty");
        }
        if (this.first == this.last) {
            this.last = null;
        }
        if (this.last != null && this.last.previous == this.first) {
            this.last.previous = null;
            this.first = this.last;
        } else {
            if (this.first.next != null) {
                this.first.next.previous = null;
            }
            this.first = this.first.next;
        }
    }

    @Override
    public void removeLast() {
        if (this.last == null) {
            throw new IllegalStateException("Stack is empty");
        }
        if (this.last == this.first) {
            this.first = null;
        }
        if (this.last.previous == this.first && this.last.previous != null) {
            this.first.next = null;
            this.last = this.first;
        } else {
            this.last = this.last.previous;
            if (this.last != null) {
                this.last.next = null;
            }
        }
    }

    private static class Entry<T> {
        private T value;
        private Entry<T> previous;
        private Entry<T> next;

        private Entry(T value, Entry<T> previous, Entry<T> next) {
            this.value = value;
            this.previous = previous;
            this.next = next;
        }
    }
}

