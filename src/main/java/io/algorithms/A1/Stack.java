package io.algorithms.A1;

import java.util.Optional;

interface Stack<T> {

    void addFirst(T element);
    void addLast(T element);
    Optional<T> peekFirst();
    Optional<T> peekLast();
    void removeFirst();
    void removeLast();
}
