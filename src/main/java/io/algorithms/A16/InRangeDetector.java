package io.algorithms.A16;

import java.util.ArrayList;
import java.util.List;

class InRangeDetector {
    static List<String> detect(List<String> rangesAndValuesToDetect) {
        int rangesCount = Integer.parseInt(rangesAndValuesToDetect.get(0));
        int valuesToDetectCount = Integer.parseInt(rangesAndValuesToDetect.get(rangesCount + 1));
        List<String> detections = new ArrayList<>();
        for (int valueIndex = rangesCount + 2; valueIndex < rangesCount + 2 + valuesToDetectCount; valueIndex++) {
            boolean isInRange = false;
            Integer value = Integer.parseInt(rangesAndValuesToDetect.get(valueIndex));
            for (int rangeIndex = 1; rangeIndex < valuesToDetectCount; rangeIndex++) {
                if (new Range(rangesAndValuesToDetect.get(rangeIndex)).isInRange(value)) {
                    isInRange = true;
                    break;
                }
            }
            if (isInRange) {
                detections.add("Yes");
            } else {
                detections.add("No");
            }
        }
        return detections;
    }

    private static class Range {
        private final int low;
        private final int high;

        Range(String range) {
            String[] lowAndHigh = range.split(" ");
            this.low = Integer.parseInt(lowAndHigh[0]);
            this.high = Integer.parseInt(lowAndHigh[1]);
        }

        boolean isInRange(Integer value) {
            return value >= this.low && value <= this.high;
        }
    }
}