package io.algorithms.A7;

import java.util.*;
import java.util.stream.Collectors;

class PairFinder {

    static List<List<Integer>> findInArrayWhichSumEqualTo(int[] array, int sum) {
        Set<Pair<Integer>> foundPairs = new HashSet<>();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i == j) continue;
                if (array[i] + array[j] == sum) {
                    if (array[i] < array[j]) {
                        foundPairs.add(new Pair<>(array[i], array[j]));
                    } else {
                        foundPairs.add(new Pair<>(array[j], array[i]));
                    }
                }
            }
        }
        return foundPairs.stream().map(Pair::toList).collect(Collectors.toList());
    }

    private static class Pair<V> {
        private final V valueOne;
        private final V valueTwo;

        Pair(V valueOne, V valueTwo) {
            this.valueOne = valueOne;
            this.valueTwo = valueTwo;
        }

        List<V> toList() {
            return Arrays.asList(valueOne, valueTwo);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair<?> pair = (Pair<?>) o;
            return Objects.equals(valueOne, pair.valueOne) && Objects.equals(valueTwo, pair.valueTwo);
        }

        @Override
        public int hashCode() {
            return Objects.hash(valueOne, valueTwo);
        }
    }
}
