package io.algorithms.A13;

class FirstUniqueLetterDetector {
    public static char detect(String word) {
        char uniqueLetter = word.charAt(0);
        for (int i = 1; i < word.length(); i++) {
            if (word.charAt(i) != uniqueLetter) {
                uniqueLetter = word.charAt(i);
                break;
            }
        }
        return uniqueLetter;
    }
}
